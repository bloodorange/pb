//
//  InfoMgr.cpp
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#include "InfoMgr.h"

static InfoMgr* __instance = NULL;

InfoMgr* InfoMgr::getInstance() {
	if(__instance == NULL) {
		__instance = new InfoMgr();
	}
	return __instance;
}

InfoMgr::InfoMgr():_nickname(NULL) {
	
}