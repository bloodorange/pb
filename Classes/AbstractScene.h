//
//  AbstractScene.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__AbstractScene__
#define __pb__AbstractScene__

#include "cocos2d.h"

class AbstraceScene : public cocos2d::Scene {
public:
	enum SceneType {
		MainMenu,
		InputUserInfo,
		SelectStage,
		GamePattern,
		GameCalc,
		GameReverse,
	};
	
	inline void setSceneType(SceneType type) { _type = type; }
	inline SceneType sceneType() { return _type; }
	
private:
	SceneType _type;
};

#endif /* defined(__pb__AbstractScene__) */
