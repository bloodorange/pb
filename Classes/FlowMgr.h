//
//  FlowMgr.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__FlowMgr__
#define __pb__FlowMgr__

#include <iostream>

namespace cocos2d {;
	class Scene;
}

class FlowMgr {
	
public:
	static FlowMgr* getInstance();
	void endScene(cocos2d::Scene *scene);
	
private:
	FlowMgr();
};

#endif /* defined(__pb__FlowMgr__) */
