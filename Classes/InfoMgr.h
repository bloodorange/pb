//
//  InfoMgr.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__InfoMgr__
#define __pb__InfoMgr__

#include <iostream>

class InfoMgr {

public:
	static InfoMgr* getInstance();
	inline void setNickname(std::string nick) { _nickname = nick; };
	inline std::string nickname() { return _nickname; };
	
private:
	InfoMgr();
	
	std::string _nickname;
};

#endif /* defined(__pb__InfoMgr__) */
