//
//  Beacon.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__Beacon__
#define __pb__Beacon__

#include "cocos2d.h"

class Beacon : public cocos2d::__NodeRGBA {

public:
	virtual bool init();
	
	inline void setText(std::string text) { _text = text; }
	inline std::string text() { return _text; }
	
	virtual void draw(cocos2d::Renderer *renderer, const kmMat4& transform, bool transformUpdated);
	
	CREATE_FUNC(Beacon);
	
private:
	std::string			_text;
	
};

#endif /* defined(__pb__Beacon__) */
