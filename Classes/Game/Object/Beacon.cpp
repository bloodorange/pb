//
//  Beacon.cpp
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#include "Beacon.h"

USING_NS_CC;

bool Beacon::init() {
	if(__NodeRGBA::init()) {
		return true;
	}
	return false;
}

void Beacon::draw(Renderer *renderer, const kmMat4& transform, bool transformUpdated) {
	
	Point begin = Point(_position.x - (_contentSize.width * _anchorPoint.x), _position.y - (_contentSize.height * _anchorPoint.y));
	Point end = Point(_position.x + (_contentSize.width * (1.f - _anchorPoint.x)), _position.y + (_contentSize.height * (1.f - _anchorPoint.y)));
	
	Color3B color = getColor();
	DrawPrimitives::drawSolidRect(begin, end, Color4F(color.r / 255.f, color.g / 255.f, color.b / 255.f, getOpacity() / 255.f));
}