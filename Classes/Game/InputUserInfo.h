//
//  InputUserInfo.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__InputUserInfo__
#define __pb__InputUserInfo__

#include "cocos2d.h"

class InputUserInfo : public cocos2d::Scene {
	public :
	virtual bool init();
	
	CREATE_FUNC(InputUserInfo);
};

#endif /* defined(__pb__InputUserInfo__) */
