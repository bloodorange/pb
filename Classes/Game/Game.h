//
//  Game.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__Game__
#define __pb__Game__

#include "cocos2d.h"

class Game : public cocos2d::Scene {
public:
//	virtual bool init();
// 	CREATE_FUNC(Game);
	
	enum GameType {
		GamePattern,
		GameCalculate,
		GameReverse,
	};
};

#endif /* defined(__pb__Game__) */
