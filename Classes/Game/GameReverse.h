//
//  GameReverse.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__GameReverse__
#define __pb__GameReverse__

#include "Game.h"

class GameReverse : public Game {
public:
	virtual bool init();
	
	CREATE_FUNC(GameReverse);
};

#endif /* defined(__pb__GameReverse__) */
