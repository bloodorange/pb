//
//  GameCalc.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__GameCalc__
#define __pb__GameCalc__

#include "Game.h"

class GameCalc : public Game {
public:
	virtual bool init();
	
	CREATE_FUNC(GameCalc);
};

#endif /* defined(__pb__GameCalc__) */
