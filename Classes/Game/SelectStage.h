//
//  SelectStage.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__SelectStage__
#define __pb__SelectStage__

#include "cocos2d.h"

class SelectStage : public cocos2d::Scene {
	public :
	virtual bool init();
	
	CREATE_FUNC(SelectStage);
};

#endif /* defined(__pb__SelectStage__) */
