//
//  GamePattern.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__GamePattern__
#define __pb__GamePattern__

#include "Game.h"

class GamePattern : public Game {
	public :
	virtual bool init();
	
	CREATE_FUNC(GamePattern);
};

#endif /* defined(__pb__GamePattern__) */
