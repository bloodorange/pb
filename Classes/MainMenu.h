//
//  MainMenu.h
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#ifndef __pb__MainMenu__
#define __pb__MainMenu__

#include "cocos2d.h"

class MainMenu : public cocos2d::Scene {
    public :
    virtual bool init();
    
    // callback
    void menuStartGameCallBack(cocos2d::Ref* pSender);
    
    CREATE_FUNC(MainMenu);
    
    private :
    void initMenu();
    cocos2d::Layer *_layer;
};

#endif /* defined(__pb__MainMenu__) */
