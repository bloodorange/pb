//
//  MainMenu.cpp
//  pb
//
//  Created by BloodOrange on 2014. 4. 20..
//
//

#include "MainMenu.h"

USING_NS_CC;

bool MainMenu::init() {
    if(Scene::init()) {
        this->initMenu();
        return true;
    }
    return false;
}

void MainMenu::initMenu() {
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    _layer = Layer::create();
    this->addChild(_layer);
    
    auto startGameItem = MenuItemFont::create("Start Game", CC_CALLBACK_1(MainMenu::menuStartGameCallBack, this));
    
    startGameItem->setPosition(Point(origin.x + visibleSize.width * 0.5,
                                     origin.y + visibleSize.height * 0.5));
    
    auto menu = Menu::create(startGameItem, NULL);
    menu->setPosition(Point::ZERO);
    _layer->addChild(menu, 1);
}

void MainMenu::menuStartGameCallBack(cocos2d::Ref *pSender) {
	
}